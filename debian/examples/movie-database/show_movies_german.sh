#!/bin/bash

recsel -e "Audio = 'German'" -p Id,Title,Audio movies.rec

# - We only want entries with German audio
# - But from them, only the Id, Title and Audio field
#   should be printed out
